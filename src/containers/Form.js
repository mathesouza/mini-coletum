import React,{Component} from 'react'
import {connect} from 'react-redux'
import Loader from 'react-loader-spinner'
import axios from 'axios'

import Fields from '../fields/Fields'

class Form extends Component{
     componentDidMount(){
        this.props.dispatch({type:'GETFORM'})
        axios.get(this.props.url)
            .then(response=>{
                this.props
                    .dispatch({
                        type:'GETFORMSUCESS',
                        payload:response.data.data.form_structure
                    })
            })
            .catch(console.log)
    }
    render(){
        const {form, loading} = this.props
        return(
            <div className='row'>
                <div className='container col-lg-6 offset-lg-3 col-sm-6 offset-sm-3 form'>
                    <form>
                        <span className='title col-md-2'>{form.name}</span>
                        <hr/>
                        {loading &&
                            <div id='loader'>
                                <Loader type="Oval" color="#19672b" height={80} width={80} />
                            </div>
                        }
                        <Fields />
                        <hr/>
                        <button className='btn btn-success'> Salvar</button>
                    </form>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state =>({
    form:state.form,
    loading: state.loading,
    url: state.url
})

export default connect(mapStateToProps)(Form)
