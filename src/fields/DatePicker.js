import React,{Component} from 'react';
import DatePicker  from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";

class DateField extends Component{
    constructor(props){
        super(props)
        this.state = {
            date: null
        }
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange(date){
        this.setState({
            date: date
        })
    }
    render(){
        return(
            <div>
                <DatePicker
                    className='form-control'
                    dateFormat="dd/MM/yyyy"
                    selected={this.state.date}
                    onChange={this.handleChange}
                />
            </div>
        )
    }
}

export default DateField
