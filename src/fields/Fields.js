import React from 'react'
import {connect} from 'react-redux'
import Rating from 'react-rating'
import DateField from './DatePicker'


const Fields = ({components,loading}) =>{
    return(
        <div>
        {!loading &&
            components.map((component,key)=>{
                return(
                    <div key={key} className='form-group'>
                        <label htmlFor={component.componentId}>{component.label} </label>
                        {component.type ==='textfield' &&
                            <input id={component.componentId} name={component.componentId} className='form-control' type='text'></input>
                        }
                        {component.type ==='datefield' &&
                            <DateField  />
                            // <input id={component.componentId} name={key} className='form-control date' type='date'></input>
                        }
                        {component.type ==='urlfield' &&
                            <input id={component.componentId} name={key} className='form-control' type='url'></input>
                        }
                        {component.type ==='ratingfield' &&
                            <div>
                                <Rating
                                    emptySymbol={<img src="images/pokebola-preta.png"  alt='empty' width='30' height='30' className="icon" />}
                                    fullSymbol={<img src="images/pokebola.png" width='30' alt='full' height='30' className="icon" />}
                                />
                            </div>
                        }
                        {component.helpBlock &&
                            <small>{component.helpBlock}</small>
                        }

                    </div>
                )
            })
        }
        </div>

    )
}
const mapStateToProps = state =>({
    loading: state.loading,
    components: state.components
})
export default connect(mapStateToProps)(Fields)
