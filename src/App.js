import React, { Component } from 'react';
import {Provider} from 'react-redux'

import Form from './containers/Form'
import store from './store/store'


class App extends Component {
    constructor(props){
        super(props)
            this.state = {
                form: null,
                components:null
            }

    }



    render() {

        return (
        <div className="">
            <Provider store={store}>
                <Form/>
            </Provider>
        </div>
        );
    }
}

export default App;
