
import { createStore } from 'redux';

const initialState = {
    form : {
        id	:"6770",
        name:	"Cadastro de Pokémon",
        status:	"enabled",
        category:	null,
        answerTracking:	true,
        publicAnswers:	true
    },
    components: [

        {
            label:	"Nome/Espécie",
            componentId:	"nomeespecie89604",
            type:	"textfield",
            helpBlock:	null,
            order:	"0",
            components:	null,
        },
        {
            label:	"Apelido carinhoso",
            componentId:	"apelidoCarinhoso89605",
            type:	"textfield",
            helpBlock:	"Invente um apelido para ele",
            order:	"1",
            components:	null,
        },
        {
            label:	"Data da captura",
            componentId:	"dataDaCaptura89876",
            type:	"datefield",
            helpBlock:	null,
            order:	"2",
            components:	null
        },
        {
            label:	"Força",
            componentId:	"forca90032",
            type:	"ratingfield",
            helpBlock:	null,
            order:	"3",
            components:	null,
        },
        {
            label:	"URL para informações do Pokémon",
            componentId:	"urlParaInformacoesDoPokemon89878",
            type:	"urlfield",
            helpBlock:	null,
            order:	"4",
            components:	null,
        }
    ],
    loading:false,
    url:'https://coletum.com/api/graphql?query={form_structure(formId:6950){label,componentId,type,helpBlock,order,components}}&token=7s5txcu909kwc48wookgw8g00occokk'
}

function formReducer(state = initialState, action){
    switch(action.type){
        case 'GETFORM':
            return Object.assign({}, state, { loading: true });
        case 'GETFORMSUCESS':
            return Object.assign({}, state, { loading: false, components:action.payload });
        default:
        return state
    }
}

const store = createStore(formReducer)

export default store
